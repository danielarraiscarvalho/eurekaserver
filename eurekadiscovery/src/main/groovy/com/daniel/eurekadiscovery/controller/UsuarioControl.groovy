package com.daniel.eurekadiscovery.controller

import com.daniel.eurekadiscovery.model.Usuario
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class UsuarioControl {

    @GetMapping
    @RequestMapping("/usuarios")
    def listUsuarios(){
        List<Usuario> usuarios = new ArrayList<>()

        usuarios.add(new Usuario("Daniel", "Arrais"))
        usuarios.add(new Usuario("Lucas", "Arrais"))
        usuarios.add(new Usuario("Mateus", "Arrais"))
        usuarios.add(new Usuario("João", "Arrais"))
        usuarios.add(new Usuario("Maria", "Arrais"))
        usuarios.add(new Usuario("Carla", "Arrais"))

        return usuarios
    }
}
