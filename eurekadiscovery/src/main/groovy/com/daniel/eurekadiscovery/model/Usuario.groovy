package com.daniel.eurekadiscovery.model

class Usuario {
    String nome
    String sobrenome

    Usuario(String nome, String sobrenome) {
        this.nome = nome
        this.sobrenome = sobrenome
    }
}
